#ifndef PHONE_CONVERSTATION_H
#define PHONE_CONVERSTATION_H
#include "constants.h"

struct date
{
    int hour;
    int minutes;
    int second;
};

struct times
{
    int year;
    int month;
    int day;
};

struct phone_converstations
{
    int reader;
    times begin;
    date begin_time;
    date duration;
    char rate[MAX_STRING_SIZE];
    double price;

};

#endif
